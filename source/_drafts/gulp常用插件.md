---
title: gulp常用插件
tags:
---
### 压缩JavaScript
gulp uglify

### 压缩HTML
gulp-htmlmin

### 压缩CSS
npm install gulp-clean-css --save-dev

### 压缩图片
npm install --save-dev gulp-image
图片压缩遇到的问题：
[Library not loaded: /usr/local/opt/libpng/lib/libpng16.16.dylib](https://github.com/tcoopman/image-webpack-loader/issues/60)