---
title: Python之setup.py
date: 2018-01-08 17:17:42
tags: [Python, setup]
---
### setup.py的packages 与 requirements.txt

### 相关链接
- [12. setup.py vs requirements.txt](http://pyzh.readthedocs.io/en/latest/python-setup-dot-py-vs-requirements-dot-txt.html)
- [第一节, setup.py 文件详解](http://www.wbh-doc.com.s3.amazonaws.com/Python-with-GitHub-PyPI-and-Readthedoc-Guide/chapter1%20-%20setup.py%20file%20guide%20for%20human.html)

### 延伸阅读
- [PyZh](http://pyzh.readthedocs.io/en/latest/index.html)