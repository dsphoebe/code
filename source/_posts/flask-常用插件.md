---
title: flask 常用插件
date: 2018-01-16 10:17:44
tags: 
  - python
  - flask
---
- flask-cache
- flask-login
- flask-mail
- flask-migrate
- flask-nav
- flask-script
- flask-sqlalchemy
- wtforms
- flask-wtf
### 补
- flask-user
- flask-restful
- flask-less
- flask-httpauth
- flask-bcrypt